#include "stm32f4xx.h"
#include "config.h"
#include "motor.h"
#include "math.h"
#include "uart.h"
#include "stdio.h"
#include "stdlib.h"

/********************************************************************************
* Macro
********************************************************************************/
#define     BUFFER_RX_SIZE  20
#define     BUFFER_TX_SIZE  22

/********************************************************************************
* Variable
********************************************************************************/

uint8_t charReceived =0x00; 

uint64_t  ENCCount =0, numCount =0; 
int start = 0; 
/************
USART variable
***************/
uint16_t          USARTBuffer[BUFFER_TX_SIZE] = {0};
uint16_t          USARTBufferRX[BUFFER_RX_SIZE] = {0}; 

/********************************************
Variable for Motor1 
******************************************/
Motor_t motor1;
 Motor_t* pMotor1 = &motor1;
Speed_control_t speedParams1;
Position_control_t positionParams1;
motor_profile_t profile1;
control_mode_t  controlMode1; 

/****************************
Variable for Motor2
**************************/
Motor_t motor2; 
 Motor_t* pMotor2 = &motor2; 
Speed_control_t speedParams2; 
Position_control_t positionParams2; 
motor_profile_t profile2; 
control_mode_t  controlMode2; 

/******************************
Pointer for usart
******************************/
float   setSpeed =0; 
uint8_t *usartSpeedPointer ; 
uint8_t *usartSpeedReceived = (uint8_t*)&(setSpeed); 


/************************
Temp Variable
************************/
uint8_t buffer[4] ={0}; 
uint8_t i =0, resetCount =0;
volatile uint8_t test =0; 
float   tempNum; 
char *data = "haha";
uint16_t lengthData;
uint16_t    state = 0; // in case that the second set position, when pMotor->pulseW = 6500, it will stop right away

/************************
Axes in cm and buffer to save setPosition
*********************/
float x= -60.0, y = -60.0;
float    bufferMotor1[2] = {0};
float    bufferMotor2[2] = {0}; 
uint16_t reset =0; 

void ConfigDMA_USART_RX(void);
void ConfigDMA_USART_TX(void); 
void ConfigInterruptDMA(void); 
float GetValueDegree(float a);
void ProcessAxes( float x, float y, float *setMotor1, float *setMotor2);
float convertToFloat(uint16_t buffer[], uint8_t index);
void assginValueToSend(uint8_t index, uint16_t buffer[], float value);
	
int main(void)
{
	uint8_t buffer[4] = {0};
	
	speedParams1.Kp = 1.9;
	speedParams1.Ki = 0.001;
	speedParams1.Kd = 0.006;
	speedParams1.speedCOF = 1;
	speedParams1.startRPM = 30;
	//speedParams1.setSpeed = -1000; 
	speedParams1.setPulse = (int)(speedParams1.setSpeed/speedParams1.speedCOF); 
	speedParams1.currentPulse = 0;
	speedParams1.currentSpeed = 0;
	profile1.startPulse = 2700;
	 
	positionParams1.setPosition = 0; 
	positionParams1.positionEOF = (float)360/(float)1330; 
	positionParams1.Kp          =  14; 
	positionParams1.Ki          = 3.5;
	positionParams1.Kd          = 32.0; 
	positionParams1.currentPosition = 0.0;
	positionParams1.iPart       = 0; 
	
	pMotor1->ENC_count[0] =0;
	pMotor1->ENC_count[1] = 0;
	pMotor1->ENC_count[2] = 0;
	pMotor1->positionParams.p_error[0] = 0; 
	pMotor1->positionParams.p_error[1] =0;
	pMotor1->positionParams.p_error[2] =0; 
	pMotor1->pGPIO        = GPIOD; 
	// chan 7 voi PD12 (forwart), chan 8 voi PD13 (reverse)
	// chan C2 voi PA15, chan C1 voi PB3
	pMotor1->pGPIOForward = GPIO_Pin_12; 
	pMotor1->pGPIOReverse = GPIO_Pin_13; 
	pMotor1->pTimCCR      = &(TIM1->CCR1);  
	pMotor1->pTimCNT      = &(TIM2->CNT); 
	
	pMotor1->controlMode = pulseToPosition;
	Motor_Init(pMotor1, speedParams1, positionParams1, profile1); 
	
	positionParams2.setPosition = 0; 
	positionParams2.positionEOF = (float)360/(float)1330; 
//	positionParams2.Kp          =  15; 
//	positionParams2.Ki          = 3;
//	positionParams2.Kd          = 20.0; 
/**********
> 2000
*************/
  positionParams2.Kp            = 18;
	positionParams2.Ki            = 3;
	positionParams2.Kd            = 25;
	positionParams2.currentPosition = 0;
	/*************
	*** 810 degree *****
	**************/
	
	pMotor2->ENC_count[0] =0;
	pMotor2->ENC_count[1] = 0;
	pMotor2->ENC_count[2] = 0;
	pMotor2->positionParams.p_error[0] = 0; 
	pMotor2->positionParams.p_error[1] =0;
	pMotor2->positionParams.p_error[2] =0; 
	pMotor2->pGPIO        = GPIOD; 
	// chan 9 voi PD15 (reverse), chan 4 voi PD14 (forward)
	// chan C2 voi PA1, chan C1 voi PA0
	pMotor2->pGPIOForward = GPIO_Pin_15; 
	pMotor2->pGPIOReverse = GPIO_Pin_14; 
	pMotor2->pTimCCR      = &(TIM1->CCR3);  
	pMotor2->pTimCNT      = &(TIM5->CNT); 
	
	pMotor2->controlMode = pulseToPosition;
	Motor_Init(pMotor2, speedParams2, positionParams2, profile2); 
	
	GPIO_init(); 
	UART_init();
	USART2_Configuration(9600); 
	ConfigDMA_USART_TX();
	ConfigDMA_USART_RX(); 
	ConfigInterruptDMA(); 
	Timer2(); 
	Timer3(); 
	Timer5(); 
	Timer1();
	Timer4();
	
	GPIO_ResetBits(GPIOD, GPIO_Pin_12);
	GPIO_ResetBits(GPIOD, GPIO_Pin_13);
	GPIO_ResetBits(GPIOD, GPIO_Pin_14);
	GPIO_ResetBits(GPIOD, GPIO_Pin_15);
	GPIO_SetBits(GPIOA, GPIO_Pin_5); 
	USART_DMACmd(USART2,USART_DMAReq_Rx,ENABLE);

  /************
	test 
	*********/
////	USART_DMACmd(USART2, USART_DMAReq_Rx, DISABLE); 
	TIM_Cmd(TIM3, DISABLE); 
	pMotor1->positionParams.setPosition = 0;
	pMotor2->positionParams.setPosition = 471; 
	
	lengthData = sizeof(data)/sizeof(char);
	setSpeed   = 0.123; 
	
	USARTBuffer[0] = (uint16_t)'p'; USARTBuffer[1] = (uint16_t)'o';
	USARTBuffer[2] = (uint16_t)'s'; USARTBuffer[3] = (uint16_t)'t';
	
  while (1)
  { 
		if (reset==1)
		{
			TIM_Cmd(TIM4, DISABLE); 
			initMotorAgain(pMotor1); 
			initMotorAgain(pMotor2); 
			delay_01ms(60000);
			initMotorAgain(pMotor1);
			initMotorAgain(pMotor2);	
			reset = 0; 			
			resetCount = 0; 
			TIM_Cmd(TIM4, ENABLE); 
		}
  }
}


float convertToFloat(uint16_t buffer[], uint8_t index)
{
	float temp;
	uint8_t i =0, num =0;
	uint8_t *numPoint = &num; 
	
	for(i =index; i< (index+4); i++)
	{
		*(numPoint++) = (uint8_t)buffer[i];
	}
	temp = *((float*)&num); 
	return temp; 
}
float GetValueDegree(float a)
{
	return a/(float)(13*3.1416); 
}

void ProcessAxes( float x, float y, float *setMotor1, float *setMotor2)
	{
		double temp = 0.0;
    double tempActan = 0.0;		
		if (x == 0)
		{
		  *setMotor1 = GetValueDegree((float)(730*y));  
      *setMotor2 = GetValueDegree((float)(730*y));			
		}
		
		else
		{
			temp = sqrt(x*x +y*y); 
			
			if ((x >0) &&(y <0))
			{
				tempActan = atan(x/y) - 3.1416/2;
				*setMotor1 = GetValueDegree((float)(12240*tempActan));
			  *setMotor2 = 0.0; 
			}
			else if ((x <0) &&(y <0))
			{
				tempActan = -(atan(x/y) +3.1416/2);
        *setMotor2 = GetValueDegree((float)(12240*tempActan));
			  *setMotor1 = 0.0; 				
			}
			else if ((x<0)&&(y>0))
			{
				tempActan = atan(x/y);
        *setMotor2  = GetValueDegree((float)(24480*tempActan));
			  *setMotor1 = 0.0; 			
			}
			else if ((x>0) && (y>0))
			{
				tempActan    = atan(x/y); 
				*setMotor1   = GetValueDegree((float)(24480*tempActan));
				*setMotor2   = 0.0; 
			}
			
			*(setMotor1+1) =  GetValueDegree((float)(730*temp));  
      *(setMotor2+1) =  GetValueDegree((float)(730*temp));  
		}
		
		ENCCount += 10000; 
	}

void assginValueToSend(uint8_t index, uint16_t buffer[], float value)
{
	uint8_t* tempNum;
	uint8_t i =0; 
	
	tempNum = (uint8_t*)(&value);  // ep kieu dia chi, de point con tro tempNum vao dau vung nho cua value. tang len 1 thi
                                 // se la gia tri byte tiep theo. 	
	for (i =index; i< (4+index); i++)
  {
		buffer[i] = *(tempNum++); 
	}		
}

void ConfigDMA_USART_TX(void)
{
	 DMA_InitTypeDef DMA_InitStruct; 
   
	 DMA_Cmd(DMA1_Stream6,DISABLE);
	DMA_InitStruct.DMA_PeripheralBaseAddr = (uint32_t)(&(USART2->DR));
	DMA_InitStruct.DMA_PeripheralInc      = DMA_PeripheralInc_Disable;
	DMA_InitStruct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStruct.DMA_PeripheralBurst    = DMA_PeripheralBurst_Single;
	DMA_InitStruct.DMA_Memory0BaseAddr    = (uint32_t)&USARTBuffer;
	DMA_InitStruct.DMA_MemoryInc          = DMA_MemoryInc_Enable;
	DMA_InitStruct.DMA_MemoryDataSize     = DMA_MemoryDataSize_HalfWord;
	DMA_InitStruct.DMA_MemoryBurst        = DMA_MemoryBurst_Single;
	DMA_InitStruct.DMA_FIFOMode           = DMA_FIFOMode_Disable;
	DMA_InitStruct.DMA_FIFOThreshold      = DMA_FIFOThreshold_HalfFull;
	DMA_InitStruct.DMA_BufferSize         = BUFFER_TX_SIZE;
	DMA_InitStruct.DMA_Channel 						= DMA_Channel_4;
	DMA_InitStruct.DMA_DIR 								= DMA_DIR_MemoryToPeripheral;
	DMA_InitStruct.DMA_Mode 							= DMA_Mode_Normal;
	DMA_InitStruct.DMA_Priority 					= DMA_Priority_High;
	DMA_Init(DMA1_Stream6,&DMA_InitStruct);
	DMA_Cmd(DMA1_Stream6,ENABLE);
	DMA_ITConfig(DMA1_Stream6,DMA_IT_TC,ENABLE);
}

void ConfigDMA_USART_RX(void)
{
	DMA_InitTypeDef DMA_InitStruct;
	
	DMA_Cmd(DMA1_Stream5,DISABLE);
	DMA_InitStruct.DMA_PeripheralBaseAddr   = (uint32_t)(&(USART2->DR));
	DMA_InitStruct.DMA_PeripheralInc 				= DMA_PeripheralInc_Disable;
	DMA_InitStruct.DMA_PeripheralDataSize 	= DMA_PeripheralDataSize_HalfWord;
	DMA_InitStruct.DMA_PeripheralBurst 			= DMA_PeripheralBurst_Single;
	DMA_InitStruct.DMA_Memory0BaseAddr 			= (uint32_t)&USARTBufferRX;
	DMA_InitStruct.DMA_MemoryInc 						= DMA_MemoryInc_Enable;
	DMA_InitStruct.DMA_MemoryDataSize 			= DMA_MemoryDataSize_HalfWord;
	DMA_InitStruct.DMA_MemoryBurst 					= DMA_MemoryBurst_Single;
	DMA_InitStruct.DMA_FIFOMode 						= DMA_FIFOMode_Disable;
	DMA_InitStruct.DMA_FIFOThreshold 				= DMA_FIFOThreshold_HalfFull;
	DMA_InitStruct.DMA_BufferSize 					= BUFFER_RX_SIZE;
	DMA_InitStruct.DMA_Channel 							= DMA_Channel_4;
	DMA_InitStruct.DMA_DIR 									= DMA_DIR_PeripheralToMemory;
	DMA_InitStruct.DMA_Mode 								= DMA_Mode_Normal;
	DMA_InitStruct.DMA_Priority 						= DMA_Priority_High;
	
	DMA_Init(DMA1_Stream5, &DMA_InitStruct);
	DMA_ITConfig(DMA1_Stream5, DMA_IT_TC,ENABLE);
	DMA_Cmd(DMA1_Stream5, ENABLE);
}

void ConfigInterruptDMA(void)
{
	NVIC_InitTypeDef NVIC_InitStruct;
	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
	
	NVIC_InitStruct.NVIC_IRQChannel = DMA1_Stream5_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
	NVIC_Init(&NVIC_InitStruct);
	
	NVIC_InitStruct.NVIC_IRQChannel = DMA1_Stream6_IRQn;
  NVIC_InitStruct.NVIC_IRQChannelSubPriority = 1;
  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStruct);
	
}


void DMA1_Stream5_IRQHandler(void) 
{
	 if(DMA_GetITStatus(DMA1_Stream5,DMA_IT_TCIF5) == SET)
		{
			DMA_ClearITPendingBit(DMA1_Stream5,DMA_IT_TCIF5);
			USART_DMACmd(USART2,USART_DMAReq_Rx,DISABLE);
			
			if ((USARTBufferRX[0] == 's') && (USARTBufferRX[1] == 't') 
					&&(USARTBufferRX[2] == 'o') && (USARTBufferRX[3]  == 'p'))
			{
				start = 0; 
				pMotor1->positionParams.setPosition = 0;
				pMotor2->positionParams.setPosition = 0; 
				initMotorAgain(pMotor1);
				initMotorAgain(pMotor2); 
				TIM_Cmd(TIM3, DISABLE);
				TIM_Cmd(TIM4, DISABLE); 
			}
			
			if ((USARTBufferRX[0] == '1') && (USARTBufferRX[1] == 's') 
					&&(USARTBufferRX[2] == 't') && (USARTBufferRX[3]  == 'a'))
			{
				x = convertToFloat(USARTBufferRX, 4);
				y = convertToFloat(USARTBufferRX, 8);
        initMotorAgain(pMotor1);
				initMotorAgain(pMotor2); 				
				ProcessAxes(x, y, bufferMotor1, bufferMotor2); 
				pMotor1->positionParams.setPosition = bufferMotor1[0];
				pMotor2->positionParams.setPosition = bufferMotor2[0]; 
				start = 1; 
				state = 1;
				TIM_Cmd(TIM3, ENABLE);
				TIM_Cmd(TIM4, ENABLE); 
			}
			
			ConfigDMA_USART_RX();
      USART_DMACmd(USART2,USART_DMAReq_Rx,ENABLE); 
		}
}


void DMA1_Stream6_IRQHandler(void)
{  
		if(DMA_GetITStatus(DMA1_Stream6,DMA_IT_TCIF6) == SET)
		{
			DMA_ClearITPendingBit(DMA1_Stream6,DMA_IT_TCIF6);
			USART_DMACmd(USART2,USART_DMAReq_Tx,DISABLE);
			ConfigDMA_USART_TX(); 
		}
}

void TIM3_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)
	{
		if (start ==1)
		{
			assginValueToSend(4, USARTBuffer, pMotor1->positionParams.setPosition);
			assginValueToSend(8, USARTBuffer, pMotor1->positionParams.currentPosition);
			assginValueToSend(12, USARTBuffer, pMotor2->positionParams.setPosition);
			assginValueToSend(16, USARTBuffer, pMotor2->positionParams.currentPosition); 
			USARTBuffer[20] = (uint16_t)'o';
			USARTBuffer[21] = (uint16_t)'n';
			USART_DMACmd(USART2, USART_DMAReq_Tx, ENABLE); 
		}
	}
	
	TIM_ClearITPendingBit(TIM3, TIM_IT_Update); 
}

#ifdef  USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  while (1)
  {}
}
#endif

