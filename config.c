#include <stm32f4xx.h>
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_tim.h>
#include <config.h>
#include <stm32f4xx_usart.h>

GPIO_InitTypeDef           GPIO_InitStructure;
NVIC_InitTypeDef           NVIC_InitStructure;
TIM_TimeBaseInitTypeDef    timInit;
EXTI_InitTypeDef           EXTI_InitStructure;
TIM_OCInitTypeDef          TIM_OCInitStructure; 
USART_InitTypeDef          USART_InitStructure;


/****************************8
*** Timer 3 for send data to usart
*******************/

void Timer3(void)
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	timInit.TIM_Period=49999;
  timInit.TIM_Prescaler=84-1;
	timInit.TIM_ClockDivision= 0;
	timInit.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseInit(TIM3, &timInit);
  TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);
	
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	TIM_Cmd(TIM3, DISABLE);
}


/************************************
**** Timer 4 for calculate PID parameters
******************/

void Timer4(void)
{	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
  timInit.TIM_Period= 29999;
  timInit.TIM_Prescaler=84-1;
	timInit.TIM_ClockDivision= 0;
	timInit.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseInit(TIM4, &timInit);
  TIM_ITConfig(TIM4,TIM_IT_Update,ENABLE);
	
	NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	TIM_Cmd(TIM4,ENABLE);
}

/***********************
*** Timer1 for PWM
******************/
void Timer1(void)
	{
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE); 
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8| GPIO_Pin_9 | GPIO_Pin_10; 
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd= GPIO_PuPd_NOPULL;
		GPIO_Init(GPIOA, &GPIO_InitStructure);
    GPIO_PinAFConfig(GPIOA,GPIO_PinSource8, GPIO_AF_TIM1);
		GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_TIM1); 
		GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_TIM1); 
		
		timInit.TIM_ClockDivision = 0; 
		timInit.TIM_CounterMode= TIM_CounterMode_Up; 
		timInit.TIM_Period = 65535; 
		timInit.TIM_Prescaler = 0; 
		TIM_TimeBaseInit(TIM1, &timInit); 
    

    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1; 
		TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
		TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
		TIM_OCInitStructure.TIM_Pulse = 0; 
		
		
		TIM_OC1Init(TIM1,&TIM_OCInitStructure); 
		TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable); 
		
		TIM_OC2Init(TIM1,&TIM_OCInitStructure); 
		TIM_OC2PreloadConfig(TIM1, TIM_OCPreload_Enable); 
		
		TIM_OC3Init(TIM1, &TIM_OCInitStructure);
    TIM_OC3PreloadConfig(TIM1, TIM_OCPreload_Enable);

		TIM_ARRPreloadConfig(TIM1,ENABLE); 
		TIM_Cmd(TIM1,ENABLE); 
		TIM_CtrlPWMOutputs(TIM1,ENABLE); 
		}
void GPIO_Configuration(void)
{
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
  /* Configure PB0 PB1 in output pushpull mode */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12 |GPIO_Pin_13 |GPIO_Pin_14 |GPIO_Pin_15;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOD, &GPIO_InitStructure);
	
	
}


/**********************
*** Timer2 for reading encoder
******************/

void Timer2(void)
{
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  /* Configure PB0 PB1 in output pushpull mode */
	// chan B cua encoder
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15 ;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
	// chan A cua encoder
	 GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3 ;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
  
  /* Time base configuration */
  timInit.TIM_Prescaler = 0;
  timInit.TIM_Period = 0xFFFFFFFF;
  timInit.TIM_ClockDivision = 0;
  timInit.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseInit(TIM2, &timInit);
	
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource3, GPIO_AF_TIM2);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource15, GPIO_AF_TIM2); 
	
   TIM_EncoderInterfaceConfig(TIM2, TIM_EncoderMode_TI12,TIM_ICPolarity_Rising, TIM_ICPolarity_Rising);
	 TIM_SetAutoreload(TIM2, 0xFFFFFFFF); 
	 TIM_Cmd(TIM2, ENABLE);
}

void Timer5(void)
{
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);
	timInit.TIM_Prescaler = 0;
  timInit.TIM_Period = 0xFFFFFFFF;
  timInit.TIM_ClockDivision = 0;
  timInit.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseInit(TIM5, &timInit);
	
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource0, GPIO_AF_TIM5);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource1, GPIO_AF_TIM5); 
	
	TIM_EncoderInterfaceConfig(TIM5, TIM_EncoderMode_TI12,TIM_ICPolarity_Rising, TIM_ICPolarity_Rising);
	TIM_SetAutoreload(TIM5, 0xFFFFFFFF); 
	TIM_Cmd(TIM5, ENABLE);
	
}
void itoa(int s, char* buffer)
{
	if ((s<1000)&&(s>-1000))
	{if (s>0)
		buffer[1] ='+';
   else 
	 {buffer[1] ='-';
	  s = -s; 
	 }		 
	buffer[0] = 'S';
	buffer[2] ='0';	
	buffer[3] = s/100 + 48;
	buffer[4] = (s - (s/100)*100)/10 + 48;
	buffer[5] = s - (s/100)*100 - ((s-(s/100)*100)/10)*10 + 48;
	buffer[6] = '0'; 
	}
	if ((s>=1000) ||(s <=-1000))
		{if (s>0)
			buffer[1] ='+';
			else 
			{buffer[1] = '-'; 
			s=-s; 
			}
		buffer[0] = 'S';
		buffer[2] = s/1000 +48; 
		buffer[3] = (s%1000)/100 +48; 
		buffer[4] = (s%100)/10 +48; 
		buffer[5] = (s%10) +48; 
    buffer[6] = '0';
	}
}

void USART_Configuration(unsigned int BaudRate)
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE); 
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
  
  /* Configure USART Tx as alternate function  */
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  /* Configure USART Rx as alternate function  */
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
  
  USART_InitStructure.USART_BaudRate = BaudRate;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  USART_Init(USART1, &USART_InitStructure);
  
  GPIO_PinAFConfig(GPIOB,GPIO_PinSource6,GPIO_AF_USART1); 
  GPIO_PinAFConfig(GPIOB,GPIO_PinSource7,GPIO_AF_USART1); 
  
  USART_Cmd(USART1, ENABLE);  
	
	// ngat 
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn ;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority =0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority=0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  USART_ITConfig(USART1, USART_IT_RXNE,ENABLE); 
}

