#include "stm32f4xx.h"
#include "stdint.h"
#include "math.h"

//***** @brief: Motor state ********

typedef enum{
		motor_state_idle,
		motor_state_running,
		motor_state_break
	} Motor_state_t;

//***** @brief: Motor speed control struct ********
	
	typedef struct{
		float currentSpeed;
		float setSpeed;
		long long int setPulse; 
		long long int currentPulse; 
		float v_error[3];
		float startRPM;
		float speedCOF;
		float Kp,Ki,Kd;
	} Speed_control_t;
	
//***** @brief: Motor position control struct ********
	
	typedef struct{
		float basePosition;
		float currentPosition;
		float setPosition;
		float Kp,Ki,Kd;
		float p_error[3];
		uint16_t numENCPerRound;
    int64_t currentPulse; 
    float positionEOF; 		
		float iPart; 
	} Position_control_t;
	
	/***************************************f
	* Control mode struct
	****************************************/
	typedef enum{
		pulseToSpeed,
		pulseToPosition,
		freeRun
	}	control_mode_t;
	
	/***************************************
	* Motor profile struct
	****************************************/
	typedef struct{
		uint32_t ENCpulsesPerRound;
		uint32_t startPulse;
		float motor_idleTime;
		float deadBand_err; 
	}	motor_profile_t;
	
	
	typedef struct{
		Speed_control_t speedParams;
		Position_control_t positionParams;
		Motor_state_t state;
		control_mode_t controlMode;
		motor_profile_t profile;
		
		__IO signed long int befPulseW;
		__IO signed long int PulseW; 
		uint64_t ENC_count[3];
		
		GPIO_TypeDef*  pGPIO;
		uint16_t       pGPIOForward;
    uint16_t       pGPIOReverse; 
    
    __IO uint32_t *pTimCCR;
		__IO uint32_t *pTimCNT;
			
	} Motor_t;
	
	/********************************************************************************
* Extern functions
********************************************************************************/
	
	extern void Motor_Speed_PID(Motor_t* pMotor);
  extern void Motor_Position_PID(Motor_t* pMotor);
  extern void Motor_Init(Motor_t* pMotor,
									Speed_control_t speedParams, Position_control_t positionParams, 
									motor_profile_t profile);
	
	static float AntiWindup(float val, float uBound, float lBound);
  static void loadPulseW(Motor_t* pMotor);
	void delay_01ms(uint16_t period);
	void initMotorAgain(Motor_t *Motor);

	